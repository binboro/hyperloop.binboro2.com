function TaskManeger(){
	this.TouchStart = false;
	this.init = function(){
		var self = this;
			this.ShowBox(),
			this.AjaxSend(),
			this.TouchOff(),
		setTimeout(function(){self.startScreen()},300);
	}
	this.AjaxGet = function(){
		var pattern = /^([a-z0-9_\.-])+@[a-z0-9-]+\.([a-z]{2,4}\.)?[a-z]{2,4}$/i;
		var msg,
			FirestName = $('#NameString').val(),
			LastName = $('#SurnameString').val(),
			formString = $('#EmailString').val(),
			RadioCheck;
			errorCounter = 0;
			$('.error_message').hide();
			$('.radio_btn').each(function(index,value){
				if($(value).prop("checked")){
					RadioCheck = $(this).next().text();
				}
			});
			 $('.rsvp-form__input').removeClass('rsvp-form__input-error');
		    if(formString == '') {
		    	errorCounter ++;
		    	$('#EmailString').addClass('rsvp-form__input-error');
		    }
			if(FirestName == ''){
				errorCounter ++;
				$('#NameString').addClass('rsvp-form__input-error');
			}
			if(LastName == ''){
				errorCounter ++;
				$('#SurnameString').addClass('rsvp-form__input-error');
			} 
		    if (errorCounter == 0) {
				if(pattern.test(formString)){
					var data = {
					msg: formString,
					name : FirestName,
					lastname: LastName,
					join : RadioCheck,
					};
					console.log(data);
						jQuery.post( 'mail.php', data , function() {
					})
					.done(function(r) {
						console.log(r);
						$('.resv-form__send-area').hide();
						$('.reserv-form__thank-block').fadeIn();

					})
					.fail(function() {
						$('.error_message').show();
					})
				}
				else{
					$('#EmailString').addClass('rsvp-form__input-error');
				}	
		    }
	}

	this.AjaxSend = function(){
		var self = this;
		$('.error_message').hide();
		$('.rvsp-from__btn').click(function(){
			self.AjaxGet();
		});

		$('#rsvp-form').submit(function(){
			self.AjaxGet();
			return false;
		});
	}
	this.ShowBox = function(){
		var self = this;
		var LastChose;
		$(document).on('tap',function(e){
			$('.circle_point__group').removeClass('circle_point__group--visible');
			if($('.circle_point__hover-area').is(e.target)){
				$(e.target).parent().find('.circle_point__group').addClass('circle_point__group--visible');
			}
			else{
				$('.circle_point__group').removeClass('circle_point__group--visible');
			}
		});
		
		$('.circle_point__hover-area').hover(function(e){
			// $(this).parent().find('.circle_point__group').animate({opacity:1},500);
			$(this).parent().find('.circle_point__group').addClass('circle_point__group--visible');
		},function(){
			// $('.circle_point__group').animate({opacity:0},500);
			$('.circle_point__group').removeClass('circle_point__group--visible');
		})
	}
	this.TouchOff = function(){
		var self = this,
			main = document.getElementById('main');
			main.addEventListener('touchstart', function(e){
				if(self.TouchStart == false){
					e.preventDefault();
				}
			}, false)
			main.addEventListener('touchmove', function(e){
				if(self.TouchStart == false){
					e.preventDefault();
				}
			}, false)
			main.addEventListener('touchend', function(e){
				if(self.TouchStart == false){
					e.preventDefault();
				}
			}, false)
	}
	this.startScreen = function(){
		var self = this,
			myBody = document.getElementsByTagName('body');
		var startText = $('.animation_text');
		setTimeout(function(){self.animeText(startText.eq(0),'#2092d2')},50);
		setTimeout(function(){self.animeText(startText.eq(1),'#2092d2')},1000);
		setTimeout(function(){self.animeText(startText.eq(2),'#2092d2')},2000);
		setTimeout(function(){$('.start_text').addClass('start_text-up')},5500);
		setTimeout(function(){$('.world').animate({opacity: 1},4000);},5500);
		setTimeout(function(){
			$('#main').addClass('main--scroll');
			self.TouchStart = true;
		},6700);
	}
	this.animeText = function(elem, color){
		var elem = $(elem);
		setTimeout(function(){
			elem.addClass("animeText1");
		},1000);
		setTimeout(function(){
			elem.addClass("animeText2");
		},1500);
		setTimeout(function(){
			elem.css("color", color).addClass("animeText3");
		},1550);
		setTimeout(function(){
			elem.removeClass("animeText3").removeClass("animeText2").removeClass("animeText1").removeClass("animeTextBlack").removeClass("animeTextWhite");
		},2000);
	};
	this.init();
};

var taskManeger = false;
$( document ).ready(function() {
	taskManeger = new TaskManeger();
});